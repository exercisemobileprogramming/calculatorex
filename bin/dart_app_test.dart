import 'dart:io';

void func1(var a, [var b]) {
  print(" Value of a is $a ");
  print(" Value of b is $b ");
}

void main() {
  // Calling the function with optional parameter
  // declaring an object s1 of the class
  var s1 = square();

  // initializing object's attribute side with value 4
  s1.side = 4;

  // calling the method area of the class. Variable a is used to hold the value returned by the function
  int a = s1.area(s1.side);

  // printing the side of the square and it’s area using string interpolation
  print(" \n Side of the square is : ${s1.side} ");
  print(" \n Area of the square is : $a ");
  print(" \n Side of the square is : ${s1.getSide()}");
  print(" \n Side of the square is : ${s1.setSide(10)}");
  print(" \n Area of the square is : ${s1.area(s1.setSide(5))} ");
}

class human {}   // this is the declaration of an empty class


class square {   // defining a class
  var side;

  int area(int r) {
    int ar = r * r;
    return ar;
  }

  int getSide(){
    return side;
  }  

  int setSide(int side){
    this.side = side;
    return side;
  }  
}

