//Test classStatic.staticVariableName;
// class Student {
//   static String course = 'null';
//   String name = 'null';
//   int enrol = 0;

//   display() {
//     print("Student's name is: $name");
//     print("Student's salary is: $enrol");
//     print("Student's branch name is: $course");
//   }
// }

// void main() {
//   Student s1 = Student();
//   Student s2 = Student();
//   Student.course = "Computer Programming"; //className.staticVariableName;
//   s1.name = "Jame Ji";
//   s1.enrol = 155;
//   s1.display();

//   s2.name = "John Smiths";
//   s2.enrol = 157;
//   s2.display();
// }



//Test super.method_name
// class Teacher {
//   var emp_name = 'James';
//   var emp_id = 1;
//   var course = 'Computer Programming';

//   void display() {
//     print("The name of the teacher is : $emp_name");
//   }
// }
// class Student extends Teacher {
//   var stu_name = 'Smith';
//   var stu_id = 1;
//   var course = 'Computer Programming';

//   void display() {
//     print("The name of the student is : $stu_name");
//     super.display();
//   }
// }

// void main() {
//   Student s1 = Student();
//   s1.display();
// }



//Test super.variable_name
// class Teacher {
//   var name = 'James';
//   var emp_id = 1;
//   var course = 'Computer Programming';
// }

// class Student extends Teacher {
//   var name = 'Smith';
//   var stu_id = 1;
//   var course = 'Computer Programming';

//   void display() {
//     print("The name of the teacher is : ");
//     print(super.name);
//     print("The name of the student is : $name");
//   }
// }

// void main() {
//   Student s1 = Student();
//   s1.display();
// }



// class Student {
  
//   Student.constructor1(int a) {
//     print(
//         ' This is the parameterized constructor with only one parameter : $a ');
//   }

//   Student.constructor2(int a, int b) {
//     print(' This is the parameterized constructor with two parameters ');
//     print(' Value of a + b is ${a + b} ');
//   }
// }

// void main() {
//   // creating two objects s1 and s2 of class student

//   Student s1 = Student.constructor1(5);

//   Student s2 = Student.constructor2(7, 8);
// }



// class Student {
//   String stuID = 'NULL';
//   String stuName = 'NULL';
//   String stuSurName = 'NULL';
//   bool giveBirthday = true;
//   String stuBirthday = 'NULL';

//   //Parameterized Constructor
//   Student(stuID, stuName, stuSurName, stuBirthday);

//   //Named Constructor
//   Student.withoutBirthday(stuID, stuName, stuSurName) : giveBirthday = false;
// }

// void main() {
//   Student s1 = Student('123', 'John', 'Smith', '1 Jan 2000');
//   Student s2 = Student.withoutBirthday('111', 'Jim', 'Null');
//   print(s1.giveBirthday);
//   print(s2);
// }



// class Addition {
//   int a = 0;
//   int b = 0;

//   int sum(int a, int b) {
//     this.a = a;
//     this.b = b;
//     return this.a + this.b;
//   }
// }

// void main() {
//   Addition a1 = new Addition();
//   int sum;

//   sum = a1.sum(5, 4);
//   print('sum = $sum');
// }
// class numbers {
//   int a = 5;
//   int b = 10;

//   numbers() {
//     print(a);
//     print(b);
//   }
// }

// void main() {
//   var n1 = numbers();
// }
