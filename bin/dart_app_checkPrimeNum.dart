import 'dart:io';

void checkPrime(int a){
  bool x = false;
  for(int i=2 ; i<a;i++){
    if(a%i==0){
      x= true;
      break;
    }
  }
  if(x==true){
    print("ไม่เป็นจำนวนเฉพาะ");
  }else{
    print("เป็นจำนวนเฉพาะ");
  }
}

void main(){
  print("กรุณาใส่ค่าลงไป : ");
  int? a = int.parse(stdin.readLineSync()!);
  checkPrime(a);
}