import 'dart:io';
import 'dart:math';

// calculate equation = calculate();
// equation.x = stdin.readLineSync()!;
// List Token = equation.stringToToken(equation.x);
// List Postfix = equation.infixToPostfix(Token);
// var Result = equation.evaluatePostfix(Postfix);

void main(){
  stdout.write("Input your Equation: ");
  calculate equation = calculate();
  equation.x = stdin.readLineSync()!;
  stdout.write("You answer is : ");
  print(equation.process());
  
}



class calculate{
  late String x ;
  
  num process(){
    List Token = stringToToken(x);
    List Postfix = infixToPostfix(Token);
    num Result = evaluatePostfix(Postfix);
    return Result;
  }

  List stringToToken(String x) {
  var temp = "";                  // เป็นตัวเก็บค่าในแต่ละ index
  var word = [];                  // คำตอบที่จะส่งกลับไป
  for(int i = 0;i<x.length;i++){
    if(x[i]==" "){                //เช็คว่า x[i] เป็นค่าจาก spacebar หรือไม่?
      if(temp.isNotEmpty){        //เช็ค temp ที่เป็นตัวเก็บค่า x[i] ก่อนหน้า ว่าไม่ใช่ ค่าว่าง จริงหรือไม่?
        word.add(temp);           //ถ้าเป็นจริงให้เพิ่มลงไปใน list ของ word
        temp ="";                 //set temp ให้เป็นค่าว่างเหมือนเดิม
      }
    }else{                       //ถ้าค่า x[i] ไม่ได้เป็นค่าที่มาจาก spacebar  หรือไม่ใช่ช่องว่าง
      temp = temp + x[i];         //ให้ temp เก็บค่า x[i] ลงไป เพื่อนำไปเพิ่มลงใน word เมื่อเงื่อนไขด้านบนเป็นจริง
    }
    if(i==x.length-1&&x[i]!=" "){ // เช็คกรณี x[i] เช็คมาจนถึงตำแหน่งตัวสุดท้ายแล้ว
      word.add(temp);
    }
  }
  return word;
}
  num evaluatePostfix(List list){
  var values = [];
  var right;
  var left;
  num result =0;
  var Operator =["+","-","*","/","^","%"];
  for(int i =0; i<list.length;i++){
    if(!Operator.contains(list[i])){
      values.add(int.parse(list[i]));
    }else{
      //remove an item from the end of values and call it right;
      right = values[values.length-1];
      values.removeLast();
      //remove an item from the end of values and call it left;
      left =values[values.length-1];
      values.removeLast();
      //check operator
      if(list[i]=="+"){ 
        result = right+left;         
        values.add(result);
      }else if(list[i]=="-"){
        result = left-right;
        values.add(result);
      }else if(list[i]=="*"){
        result = right*left;
        values.add(result);
      }else if(list[i]=="/"){
        result = left/right;
        values.add(result);
      }else if(list[i]=="^"){
        result = pow(left, right);
        values.add(result);
      }else if(list[i]=="%"){
        result = left % right ;
        values.add(result);
      }

    }
  }
  return values[0];          //Return the first item in values as the of the expression
 }

  //Infix to Posfixx
List infixToPostfix(List x){
  var listOp =[];
  var listPost = [];
  var Operator =["+","-","*","/","^","%"];
  var precedence = {"+":1,"-":1, "*":2, "/": 2, "^":3, "%":3}; //เลขมากกว่า สำคัญกว่า
                                                                      //4. ดำเนินการในส่วนที่อยู่ในวงเล็บก่อน (... )
                                                                      //3. ตามมาด้วยดำเนินการในส่วนที่เป็นเลขยกกำลัง หรือราก [n^, √]
                                                                      //2. จากนั้นดำเนินการในส่วนที่เป็นการคูณและหารทั้งหมด × / ÷ 
                                                                      //   โดยแก้สมการทางคณิตศาสตร์จากซ้ายไปขวา
                                                                      //1. ดำเนินการสุดท้ายเสมอคือการบวกและลบทั้งหมด + / – 
                                                                      //   โดยแก้สมการทางคณิตศาสตร์จากซ้ายไปขวาเช่นกัน
  for(int i=0;i<x.length;i++){
    //Check Operator
    if(Operator.contains(x[i])){
      while(listOp.isNotEmpty && listOp[listOp.length-1]!=("(") &&
            precedence[x[i]]! <=precedence[listOp[listOp.length-1]]!){
        listPost.add(listOp[listOp.length-1]);
        listOp.removeLast();
      }
      listOp.add(x[i]);
    }
    //Check the parenthisis
    else if(x[i]=="("||x[i]==")"){
      if(x[i]=="("){
        listOp.add(x[i]);
      }else if(x[i]==")"){
        while(listOp[listOp.length-1]!="("){
          listPost.add(listOp[listOp.length-1]);
          listOp.removeLast();
        }
      listOp.removeLast();
      }
    }else{   //check number or operand.
      if(int.parse(x[i]) is int){
        listPost.add(x[i]);
      }
    }
  }
  while(listOp.isNotEmpty){
    listPost.add(listOp[listOp.length-1]);
    listOp.removeLast();
  }
  return listPost;

}

}


